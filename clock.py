import os
import time #since there is no simple and short way to get the UTC offset in datetime, I’ll stick to this solution
import datetime

while True:
	l_lTime = datetime.datetime.now().time()
	t_gTime = datetime.datetime.utcnow().time()
	i_IDLTime = l_lTime.hour + (time.timezone/60/60 - 12) #get the offset time for UTC+12 in hours

	if(i_IDLTime < 0): i_IDLTime += 24 #and if the time is presented in an incorrect pitch (-GRNWCH), slam it back to the correct day

	#these two are floats that represent the cent amount of time (basically, how many 1/100s have passed since 00:00)
	#in order to show the time in decs or mils you have to change the power of 10 to 1 or 3 accordingly
	f_LMT = (
		(l_lTime.second/86400) +
		(l_lTime.minute/1440) +
		(l_lTime.hour/24)
	)*10**2

	f_UMT = (
		(l_lTime.second/86400) +
		(l_lTime.minute/1440) +
		(i_IDLTime/24)
	)*10**2

	os.system('cls' if os.name == 'nt' else 'clear')

	print('UTC: {0:02d}:{1:02d}:{0:02d}'.format(t_gTime.hour, t_gTime.minute, t_gTime.second))
	print('ABT: {0} (TZ: UTC{1:.0f})'.format(time.strftime("%H:%M:%S"), -time.timezone/60/60))
	print('LMT: {0:.3f}'.format(f_LMT))
	print('UMT: {0:.3f}'.format(f_UMT))

	time.sleep(1)